# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [5.0.1]
### Added
- Add missing language label for Variant slugs

## [5.0.0]
### Added
- Add CI tools for gitlab.
- Add dedicated show view plugin.
- Refactor code base.
- Remove entities Price, ScaledPrice and Discount.
- Remove ke_search hooks.
- Remove dead files (js, css, etc..)
- Remove category plugin.
- Remove switchable controller actions.

## [4.0.0]
### Added
- Add support for TYPO3 10.4

## [2.0.0]
### Added
- Add support for TYPO3 8.7
