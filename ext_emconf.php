<?php
/**
 * This file is part of the "rico_products" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2020 PSVneo
 */

$EM_CONF[\Riconet\RicoProducts\Constants::EXTENSION_KEY] = [
    'title' => 'Products',
    'description' => 'This extension provides a basic system to manage product data.',
    'version' => '5.0.1',
    'category' => 'plugin',
    'constraints' => [
        'depends' => [
            'typo3' => '10.4.0-10.4.99',
        ]
    ],
    'state' => 'stable',
    'uploadfolder' => false,
    'clearCacheOnLoad' => true,
    'author' => 'PSVneo',
    'author_email' => 'hello@psv-neo.de',
    'author_company' => 'PSVneo GmbH',
    'autoload' => [
        'psr-4' => [
            'Riconet\\RicoProducts\\' => 'Classes',
        ],
    ],
    'autoload-dev' => [
        'psr-4' => [
            'Riconet\\RicoProducts\\Tests\\' => 'Tests',
        ],
    ],
];
