<?php

/**
 * This file is part of the "rico_products" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2022 PSVneo GmbH
 */

declare(strict_types=1);

namespace Riconet\RicoProducts\Domain\Repository;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\Generic\Qom\ConstraintInterface;
use TYPO3\CMS\Extbase\Persistence\QueryInterface;
use TYPO3\CMS\Extbase\Persistence\QueryResultInterface;
use TYPO3\CMS\Extbase\Persistence\Repository;

class ProductRepository extends Repository
{
    public function findBySettings(array $settings): QueryResultInterface
    {
        $query = $this->createQuery();
        $constrains = $this->addSettingsConstraints($settings, $query);
        if (count($constrains) > 0) {
            $query->matching($query->logicalAnd($constrains));
        }
        /** @var QueryResultInterface $result */
        $result = $query->execute();

        return $result;
    }

    /**
     * @return ConstraintInterface[]
     */
    protected function addSettingsConstraints(array $settings, QueryInterface $query): array
    {
        $constraints = [];

        // Handle categories constraints.
        if (! empty($settings['constraints']['categories'] ?? '')) {
            $constraints[] = $this->addCategoriesConstraints(
                GeneralUtility::intExplode(',', $settings['constraints']['categories'], true),
                $query
            );
        }

        return $constraints;
    }

    /**
     * @param int[] $categoryUids
     */
    protected function addCategoriesConstraints(array $categoryUids, QueryInterface $query): ConstraintInterface
    {
        $constraints = [];
        foreach ($categoryUids as $categoryUid) {
            $constraints[] = $query->contains('categories', $categoryUid);
        }

        return $query->logicalAnd(...$constraints);
    }
}
