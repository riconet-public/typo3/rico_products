<?php

/**
 * This file is part of the "rico_products" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2022 PSVneo GmbH
 */

declare(strict_types=1);

namespace Riconet\RicoProducts;

class Constants
{
    public const EXTENSION_KEY = 'rico_products';

    public const EXTENSION_NAME = 'RicoProducts';

    public const PLUGIN_NAME_PRODUCT_LISTING = 'ProductListing';

    public const PLUGIN_NAME_PRODUCT_SHOW = 'ProductShow';
}
