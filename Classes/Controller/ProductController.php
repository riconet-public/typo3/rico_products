<?php

/**
 * This file is part of the "rico_products" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2022 PSVneo GmbH
 */

declare(strict_types=1);

namespace Riconet\RicoProducts\Controller;

use Riconet\RicoProducts\Domain\Model\Product;
use Riconet\RicoProducts\Domain\Repository\ProductRepository;
use Riconet\RicoProducts\Domain\Service\RecordPaginatorInterface;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;

class ProductController extends ActionController
{
    protected ProductRepository $productRepository;

    protected RecordPaginatorInterface $paginator;

    public function __construct(ProductRepository $productRepository, RecordPaginatorInterface $paginator)
    {
        $this->productRepository = $productRepository;
        $this->paginator = $paginator;
    }

    public function listAction(int $page = 1): void
    {
        $page = $page <= 0 ? 1 : $page;
        $this->paginator->setup(
            $this->productRepository->findBySettings($this->settings),
            $page,
            intval($this->settings['pagination']['itemsPerPage'] ?? 10),
            intval($this->settings['pagination']['maximumNumberOfLinks'] ?? 10),
        );
        $this->view->assignMultiple($this->paginator->getValues());
    }

    public function showAction(Product $product): void
    {
        $this->view->assign('product', $product);
    }
}
