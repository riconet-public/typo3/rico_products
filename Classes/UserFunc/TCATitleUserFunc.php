<?php

/**
 * This file is part of the "rico_products" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2022 PSVneo GmbH
 */

declare(strict_types=1);

namespace Riconet\RicoProducts\UserFunc;

use TYPO3\CMS\Backend\Utility\BackendUtility;

class TCATitleUserFunc
{
    public function formatAttributeLabel(array &$parameters): void
    {
        $attributeData = BackendUtility::getRecord(
            $parameters['table'] ?? '',
            $parameters['row']['uid'] ?? 0
        );

        if (! is_array($attributeData)) {
            return;
        }

        $attribute = BackendUtility::getRecord(
            'tx_ricoproducts_domain_model_attribute',
            intval($attributeData['attribute'] ?? 0)
        );

        if (! is_array($attribute)) {
            return;
        }

        $title = $attribute['title'] ?? '';
        $data = $attributeData['data'] ?? '';
        $unit = $attribute['unit'] ?? '';
        $parameters['title'] = $title . ': ' . $data . ' ' . $unit;
    }
}
