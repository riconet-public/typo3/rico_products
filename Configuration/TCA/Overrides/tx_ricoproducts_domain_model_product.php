<?php

/**
 * This file is part of the "rico_products" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2020 PSVneo
 */

declare(strict_types=1);

defined('TYPO3_MODE') || die();

(function (string $extensionKey, string $table) {
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::makeCategorizable($extensionKey, $table, 'categories', [
        'exclude' => false,
        'fieldConfiguration' => [
            'behaviour' => [
                'allowLanguageSynchronization' => true,
            ]
        ]
    ]);
}
)('rico_products', 'tx_ricoproducts_domain_model_product');
