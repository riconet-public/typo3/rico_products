<?php

/**
 * This file is part of the "rico_products" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2020 PSVneo
 */

declare(strict_types=1);

defined('TYPO3_MODE') || die();

return (function ($extensionKey, $table) {
    $ll = "LLL:EXT:$extensionKey/Resources/Private/Language/locallang_db.xlf";
    $fields = implode(',', [
        'product',
        'variant',
        'attribute',
        'data',
    ]);

    return [
        'ctrl' => [
            'title' => "$ll:$table",
            'label' => 'data',
            'label_userFunc' => \Riconet\RicoProducts\UserFunc\TCATitleUserFunc::class . '->formatAttributeLabel',
            'hideTable' => true,
            'tstamp' => 'tstamp',
            'crdate' => 'crdate',
            'cruser_id' => 'cruser_id',
            'versioningWS' => true,
            'origUid' => 't3_origuid',
            'languageField' => 'sys_language_uid',
            'transOrigPointerField' => 'l10n_parent',
            'transOrigDiffSourceField' => 'l10n_diffsource',
            'default_sortby' => 'data',
            'delete' => 'deleted',
            'enablecolumns' => [
                'disabled' => 'hidden',
                'starttime' => 'starttime',
                'endtime' => 'endtime',
            ],
            'searchFields' => 'data',
            'iconfile' => "EXT:$extensionKey/Resources/Public/Icons/$table.svg",
        ],
        'types' => [
            '1' => [
                'showitem' => implode(',', [
                    $fields,
                    '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language',
                    '--palette--;;language',
                    '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access',
                    'hidden,--palette--;;timeRestriction',
                ]),
            ],
        ],
        'palettes' => [
            'timeRestriction' => ['showitem' => 'starttime, endtime'],
            'language' => ['showitem' => 'sys_language_uid, l10n_parent'],
        ],
        'columns' => [
            'sys_language_uid' => [
                'exclude' => true,
                'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.language',
                'config' => [
                    'type' => 'select',
                    'renderType' => 'selectSingle',
                    'special' => 'languages',
                    'items' => [
                        [
                            'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.allLanguages',
                            -1,
                            'flags-multiple'
                        ],
                    ],
                    'default' => 0,
                ]
            ],
            'l10n_parent' => [
                'displayCond' => 'FIELD:sys_language_uid:>:0',
                'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.l18n_parent',
                'config' => [
                    'type' => 'select',
                    'renderType' => 'selectSingle',
                    'items' => [
                        ['', 0]
                    ],
                    'foreign_table' => $table,
                    'foreign_table_where' => "AND $table.pid=###CURRENT_PID### AND $table.sys_language_uid IN (-1,0)",
                    'default' => 0
                ]
            ],
            'l10n_diffsource' => [
                'config' => [
                    'type' => 'passthrough',
                    'default' => ''
                ]
            ],
            'hidden' => [
                'exclude' => true,
                'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.enabled',
                'config' => [
                    'type' => 'check',
                    'renderType' => 'checkboxToggle',
                    'items' => [
                        [
                            0 => '',
                            1 => '',
                            'invertStateDisplay' => true
                        ]
                    ],
                ]
            ],
            'starttime' => [
                'exclude' => true,
                'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.starttime',
                'config' => [
                    'type' => 'input',
                    'renderType' => 'inputDateTime',
                    'eval' => 'datetime,int',
                    'default' => 0,
                    'behaviour' => [
                        'allowLanguageSynchronization' => true,
                    ]
                ]
            ],
            'endtime' => [
                'exclude' => true,
                'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.endtime',
                'config' => [
                    'type' => 'input',
                    'renderType' => 'inputDateTime',
                    'eval' => 'datetime,int',
                    'default' => 0,
                    'range' => [
                        'upper' => mktime(0, 0, 0, 1, 1, 2038),
                    ],
                    'behaviour' => [
                        'allowLanguageSynchronization' => true,
                    ]
                ]
            ],
            'data' => [
                'exclude' => true,
                'label' => "$ll:$table.data",
                'config' => [
                    'type' => 'input',
                    'size' => 30,
                    'eval' => 'trim',
                    'behaviour' => [
                        'allowLanguageSynchronization' => true,
                    ],
                ],
            ],
            'attribute' => [
                'exclude' => true,
                'l10n_display' => 'defaultAsReadonly',
                'label' => "$ll:$table.attribute",
                'config' => [
                    'type' => 'select',
                    'renderType' => 'selectSingle',
                    'foreign_table' => 'tx_ricoproducts_domain_model_attribute',
                    'foreign_table_where' => 'tx_ricoproducts_domain_model_attribute.sys_language_uid IN (0,-1)',
                    'size' => 1,
                    'maxitems' => 1,
                    'eval' => 'required',
                ],
            ],
            'product' => [
                'config' => [
                    'type' => 'passthrough',
                ],
            ],
            'variant' => [
                'config' => [
                    'type' => 'passthrough',
                ],
            ],
        ],
    ];
})(
    \Riconet\RicoProducts\Constants::EXTENSION_KEY,
    'tx_ricoproducts_domain_model_attributedata'
);
