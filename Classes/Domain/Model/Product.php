<?php

/**
 * This file is part of the "rico_products" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2022 PSVneo GmbH
 */

declare(strict_types=1);

namespace Riconet\RicoProducts\Domain\Model;

use TYPO3\CMS\Extbase\Annotation\ORM\Cascade;
use TYPO3\CMS\Extbase\Annotation\ORM\Lazy;
use TYPO3\CMS\Extbase\Annotation\Validate;
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

class Product extends AbstractEntity
{
    /**
     * @Validate("NotEmpty")
     */
    protected string $title = '';

    protected ?string $subtitle = '';

    /**
     * @Validate("NotEmpty")
     */
    protected ?string $articleNumber = '';

    protected ?string $teaser = '';

    protected ?string $description = '';

    protected ?float $price = 0.00;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference>
     *
     * @Cascade("remove")
     *
     * @Lazy()
     */
    protected $images;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference>
     *
     * @Cascade("remove")
     *
     * @Lazy()
     */
    protected $planImages;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference>
     *
     * @Cascade("remove")
     *
     * @Lazy()
     */
    protected $dataSheets;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference>
     *
     * @Cascade("remove")
     *
     * @Lazy()
     */
    protected $manuals;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference>
     *
     * @Cascade("remove")
     *
     * @Lazy()
     */
    protected $files;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Riconet\RicoProducts\Domain\Model\AttributeData>
     *
     * @Cascade("remove")
     *
     * @Lazy()
     */
    protected $attributes;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Riconet\RicoProducts\Domain\Model\Variant>
     *
     * @Cascade("remove")
     *
     * @Lazy()
     */
    protected $variants;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\Category>
     *
     * @Lazy()
     */
    protected $categories;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Riconet\RicoProducts\Domain\Model\Product>
     *
     * @Lazy()
     */
    protected $accessories;

    public function __construct()
    {
        $this->initStorageObjects();
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function getSubtitle(): ?string
    {
        return $this->subtitle;
    }

    public function setSubtitle(?string $subtitle): void
    {
        $this->subtitle = $subtitle;
    }

    public function getArticleNumber(): ?string
    {
        return $this->articleNumber;
    }

    public function setArticleNumber(?string $articleNumber): void
    {
        $this->articleNumber = $articleNumber;
    }

    public function getTeaser(): ?string
    {
        return $this->teaser;
    }

    public function setTeaser(?string $teaser): void
    {
        $this->teaser = $teaser;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(?float $price): void
    {
        $this->price = $price;
    }

    public function getImages(): ObjectStorage
    {
        return $this->images;
    }

    public function setImages(ObjectStorage $images): void
    {
        $this->images = $images;
    }

    public function getPlanImages(): ObjectStorage
    {
        return $this->planImages;
    }

    public function setPlanImages(ObjectStorage $planImages): void
    {
        $this->planImages = $planImages;
    }

    public function getDataSheets(): ObjectStorage
    {
        return $this->dataSheets;
    }

    public function setDataSheets(ObjectStorage $dataSheets): void
    {
        $this->dataSheets = $dataSheets;
    }

    public function getManuals(): ObjectStorage
    {
        return $this->manuals;
    }

    public function setManuals(ObjectStorage $manuals): void
    {
        $this->manuals = $manuals;
    }

    public function getFiles(): ObjectStorage
    {
        return $this->files;
    }

    public function setFiles(ObjectStorage $files): void
    {
        $this->files = $files;
    }

    public function getAttributes(): ?ObjectStorage
    {
        return $this->attributes;
    }

    public function setAttributes(ObjectStorage $attributes): void
    {
        $this->attributes = $attributes;
    }

    public function getVariants(): ObjectStorage
    {
        return $this->variants;
    }

    public function setVariants(ObjectStorage $variants): void
    {
        $this->variants = $variants;
    }

    public function getCategories(): ObjectStorage
    {
        return $this->categories;
    }

    public function setCategories(ObjectStorage $categories): void
    {
        $this->categories = $categories;
    }

    public function getAccessories(): ObjectStorage
    {
        return $this->accessories;
    }

    public function setAccessories(ObjectStorage $accessories): void
    {
        $this->accessories = $accessories;
    }

    protected function initStorageObjects(): void
    {
        $this->images = new ObjectStorage();
        $this->planImages = new ObjectStorage();
        $this->files = new ObjectStorage();
        $this->dataSheets = new ObjectStorage();
        $this->manuals = new ObjectStorage();
        $this->attributes = new ObjectStorage();
        $this->variants = new ObjectStorage();
        $this->categories = new ObjectStorage();
        $this->accessories = new ObjectStorage();
    }
}
