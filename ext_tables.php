<?php
/**
 * This file is part of the "rico_products" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2020 PSVneo
 */

defined('TYPO3_MODE') || die();

(function ($extensionKey) {
    $tables = [
        'tx_ricoproducts_domain_model_product',
        'tx_ricoproducts_domain_model_variant',
        'tx_ricoproducts_domain_model_attribute',
        'tx_ricoproducts_domain_model_attributedata'
    ];
    // Add TCA descriptions and allow on standard tables.
    foreach ($tables as $table) {
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages($table);
    }
}
)('rico_products');
