<?php
/**
 * This file is part of the "rico_products" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2020 PSVneo
 */

defined('TYPO3_MODE') || die();

(function ($extensionKey) {
    // Configure product listing plugin.
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        $extensionKey,
        \Riconet\RicoProducts\Constants::PLUGIN_NAME_PRODUCT_LISTING,
        [
            \Riconet\RicoProducts\Controller\ProductController::class => 'list',
        ]
    );
    // Configure product show plugin.
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        $extensionKey,
        \Riconet\RicoProducts\Constants::PLUGIN_NAME_PRODUCT_SHOW,
        [
            \Riconet\RicoProducts\Controller\ProductController::class => 'show',
        ]
    );
}
)('rico_products');
