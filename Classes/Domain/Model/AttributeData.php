<?php

/**
 * This file is part of the "rico_products" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2022 PSVneo GmbH
 */

declare(strict_types=1);

namespace Riconet\RicoProducts\Domain\Model;

use TYPO3\CMS\Extbase\Annotation\Validate;
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

class AttributeData extends AbstractEntity
{
    protected ?string $data = '';

    /**
     * @var \Riconet\RicoProducts\Domain\Model\Attribute|null
     *
     * @Validate("NotEmpty")
     */
    protected ?Attribute $attribute = null;

    /**
     * @var \Riconet\RicoProducts\Domain\Model\Product|null
     */
    protected ?Product $product = null;

    /**
     * @var \Riconet\RicoProducts\Domain\Model\Variant|null
     */
    protected ?Variant $variant = null;

    public function getData(): ?string
    {
        return $this->data;
    }

    public function setData(?string $data): void
    {
        $this->data = $data;
    }

    public function getAttribute(): ?Attribute
    {
        return $this->attribute;
    }

    public function setAttribute(?Attribute $attribute): void
    {
        $this->attribute = $attribute;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): void
    {
        $this->product = $product;
    }

    public function getVariant(): ?Variant
    {
        return $this->variant;
    }

    public function setVariant(?Variant $variant): void
    {
        $this->variant = $variant;
    }
}
