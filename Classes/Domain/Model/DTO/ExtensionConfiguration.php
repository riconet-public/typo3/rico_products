<?php

/**
 * This file is part of the "rico_products" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2022 PSVneo GmbH
 */

declare(strict_types=1);

namespace Riconet\RicoProducts\Domain\Model\DTO;

use Exception;
use Riconet\RicoProducts\Constants;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class ExtensionConfiguration
{
    private string $productSlugBehaviour = '';

    public function __construct()
    {
        try {
            /** @var \TYPO3\CMS\Core\Configuration\ExtensionConfiguration $extensionConfiguration */
            $extensionConfiguration = GeneralUtility::makeInstance(\TYPO3\CMS\Core\Configuration\ExtensionConfiguration::class);
            $configuration = (array) $extensionConfiguration->get(Constants::EXTENSION_KEY);
            foreach ($configuration as $key => $value) {
                $methodName = 'set' . ucfirst((string) $key);
                if (method_exists(__CLASS__, $methodName)) {
                    $this->$methodName = $value;
                }
            }
        } catch (Exception $exception) {
            // do nothing
        }
    }

    public function getProductSlugBehaviour(): string
    {
        return $this->productSlugBehaviour;
    }

    public function setProductSlugBehaviour(string $productSlugBehaviour): void
    {
        $this->productSlugBehaviour = $productSlugBehaviour;
    }
}
