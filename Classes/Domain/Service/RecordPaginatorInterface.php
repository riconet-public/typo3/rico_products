<?php

/**
 * This file is part of the "rico_products" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2022 PSVneo GmbH
 */

declare(strict_types=1);

namespace Riconet\RicoProducts\Domain\Service;

use TYPO3\CMS\Extbase\Pagination\QueryResultPaginator;
use TYPO3\CMS\Extbase\Persistence\QueryResultInterface;

interface RecordPaginatorInterface
{
    public function setup(
        QueryResultInterface $queryResult,
        int $currentPage,
        int $itemsPerPage,
        int $maximumNumberOfLinks
    ): void;

    public function getPaginator(): ?QueryResultPaginator;

    public function getValues(): array;
}
