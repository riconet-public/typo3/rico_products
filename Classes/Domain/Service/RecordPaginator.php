<?php

/**
 * This file is part of the "rico_products" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2022 PSVneo GmbH
 */

declare(strict_types=1);

namespace Riconet\RicoProducts\Domain\Service;

use TYPO3\CMS\Core\Pagination\AbstractPaginator;
use TYPO3\CMS\Extbase\Pagination\QueryResultPaginator;
use TYPO3\CMS\Extbase\Persistence\QueryResultInterface;

class RecordPaginator implements RecordPaginatorInterface
{
    private ?QueryResultPaginator $paginator = null;

    private int $itemsPerPage = 10;

    private int $maximumNumberOfLinks = 10;

    public function setup(
        QueryResultInterface $queryResult,
        int $currentPage,
        int $itemsPerPage,
        int $maximumNumberOfLinks
    ): void {
        $this->itemsPerPage = $itemsPerPage;
        $this->maximumNumberOfLinks = $maximumNumberOfLinks;
        $this->paginator = new QueryResultPaginator($queryResult, $currentPage, $this->itemsPerPage);
    }

    public function getPaginator(): ?QueryResultPaginator
    {
        return $this->paginator;
    }

    public function getValues(): array
    {
        if (is_null($this->paginator)) {
            return [];
        }

        return [
            'pagination' => [
                'paginatedRecords' => $this->paginator->getPaginatedItems(),
                'itemsPerPage' => $this->itemsPerPage,
                'numberOfPages' => $this->paginator->getNumberOfPages(),
                'maximumNumberOfLinks' => $this->maximumNumberOfLinks,
                'page' => [
                    'current' => $this->paginator->getCurrentPageNumber(),
                    'first' => 1,
                    'last' => $this->paginator->getNumberOfPages(),
                    'next' => $this->getNextPage($this->paginator),
                    'prev' => $this->getPreviousPage($this->paginator),
                    'range' => range(1, $this->paginator->getNumberOfPages()),
                ],
            ],
        ];
    }

    private function getPreviousPage(AbstractPaginator $pagination): int
    {
        $prev = $pagination->getCurrentPageNumber() - 1;

        return $prev > 0 ? $prev : 1;
    }

    private function getNextPage(AbstractPaginator $pagination): int
    {
        $next = $pagination->getCurrentPageNumber() + 1;

        return $next <= $pagination->getNumberOfPages() ? $next : $pagination->getNumberOfPages();
    }
}
