<?php

/**
 * This file is part of the "rico_products" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2020 PSVneo
 */

declare(strict_types=1);

defined('TYPO3_MODE') || die();

return (function ($extensionKey, $table) {
    /** @var \Riconet\RicoProducts\Domain\Model\DTO\ExtensionConfiguration $extensionConfiguration */
    $extensionConfiguration = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(
        \Riconet\RicoProducts\Domain\Model\DTO\ExtensionConfiguration::class
    );
    $ll = "LLL:EXT:$extensionKey/Resources/Private/Language/locallang_db.xlf";
    $fields = implode(',', [
        'article_number',
        'title',
        'path_segment',
        'subtitle',
        'description',
        'price',
        'attributes',
        'images',
    ]);

    return [
        'ctrl' => [
            'title' => "$ll:$table",
            'label' => 'article_number',
            'hideTable' => true,
            'tstamp' => 'tstamp',
            'crdate' => 'crdate',
            'cruser_id' => 'cruser_id',
            'versioningWS' => true,
            'origUid' => 't3_origuid',
            'languageField' => 'sys_language_uid',
            'transOrigPointerField' => 'l10n_parent',
            'transOrigDiffSourceField' => 'l10n_diffsource',
            'default_sortby' => 'article_number',
            'delete' => 'deleted',
            'enablecolumns' => [
                'disabled' => 'hidden',
                'starttime' => 'starttime',
                'endtime' => 'endtime',
            ],
            'searchFields' => $fields,
            'iconfile' => "EXT:$extensionKey/Resources/Public/Icons/$table.svg",
        ],
        'types' => [
            '1' => [
                'showitem' => implode(',', [
                    'article_number',
                    'title',
                    'path_segment',
                    'subtitle',
                    'description',
                    "--div--;$ll:tx_ricoproducts_tabs.pricing",
                    'price',
                    "--div--;$ll:tx_ricoproducts_tabs.attributes",
                    'attributes',
                    "--div--;$ll:tx_ricoproducts_tabs.files",
                    'images',
                    '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language',
                    '--palette--;;language',
                    '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access',
                    'hidden,--palette--;;timeRestriction',
                ]),
            ],
        ],
        'palettes' => [
            'timeRestriction' => ['showitem' => 'starttime, endtime'],
            'language' => ['showitem' => 'sys_language_uid, l10n_parent'],
        ],
        'columns' => [
            'sys_language_uid' => [
                'exclude' => true,
                'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.language',
                'config' => [
                    'type' => 'select',
                    'renderType' => 'selectSingle',
                    'special' => 'languages',
                    'items' => [
                        [
                            'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.allLanguages',
                            -1,
                            'flags-multiple'
                        ],
                    ],
                    'default' => 0,
                ]
            ],
            'l10n_parent' => [
                'displayCond' => 'FIELD:sys_language_uid:>:0',
                'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.l18n_parent',
                'config' => [
                    'type' => 'select',
                    'renderType' => 'selectSingle',
                    'items' => [
                        ['', 0]
                    ],
                    'foreign_table' => $table,
                    'foreign_table_where' => "AND $table.pid=###CURRENT_PID### AND $table.sys_language_uid IN (-1,0)",
                    'default' => 0
                ]
            ],
            'l10n_diffsource' => [
                'config' => [
                    'type' => 'passthrough',
                    'default' => ''
                ]
            ],
            'hidden' => [
                'exclude' => true,
                'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.enabled',
                'config' => [
                    'type' => 'check',
                    'renderType' => 'checkboxToggle',
                    'items' => [
                        [
                            0 => '',
                            1 => '',
                            'invertStateDisplay' => true
                        ]
                    ],
                ]
            ],
            'starttime' => [
                'exclude' => true,
                'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.starttime',
                'config' => [
                    'type' => 'input',
                    'renderType' => 'inputDateTime',
                    'eval' => 'datetime,int',
                    'default' => 0,
                    'behaviour' => [
                        'allowLanguageSynchronization' => true,
                    ]
                ]
            ],
            'endtime' => [
                'exclude' => true,
                'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.endtime',
                'config' => [
                    'type' => 'input',
                    'renderType' => 'inputDateTime',
                    'eval' => 'datetime,int',
                    'default' => 0,
                    'range' => [
                        'upper' => mktime(0, 0, 0, 1, 1, 2038),
                    ],
                    'behaviour' => [
                        'allowLanguageSynchronization' => true,
                    ]
                ]
            ],
            'title' => [
                'exclude' => true,
                'label' => "$ll:$table.title",
                'config' => [
                    'type' => 'input',
                    'size' => 30,
                    'eval' => 'trim',
                ],
            ],
            'path_segment' => [
                'label' =>"$ll:$table.path_segment",
                'config' => [
                    'type' => 'slug',
                    'size' => 50,
                    'generatorOptions' => [
                        'fields' => ['title'],
                        'replacements' => [
                            '/' => '-'
                        ],
                    ],
                    'fallbackCharacter' => '-',
                    'eval' => $extensionConfiguration->getProductSlugBehaviour(),
                    'default' => ''
                ]
            ],
            'subtitle' => [
                'exclude' => true,
                'label' => "$ll:tx_ricoproducts_domain_model_product.subtitle",
                'config' => [
                    'type' => 'text',
                    'cols' => 40,
                    'rows' => 2,
                    'eval' => 'trim',
                ],
            ],
            'article_number' => [
                'label' => "$ll:$table.article_number",
                'config' => [
                    'type' => 'input',
                    'size' => 4,
                    'eval' => 'trim,required',
                    'behaviour' => [
                        'allowLanguageSynchronization' => true,
                    ],
                ],
            ],
            'description' => [
                'exclude' => true,
                'label' => "$ll:tx_ricoproducts_domain_model_product.description",
                'config' => [
                    'type' => 'text',
                    'cols' => 40,
                    'rows' => 3,
                    'enableRichtext' => 1,
                ],
            ],
            'images' => [
                'exclude' => true,
                'label' => "$ll:$table.images",
                'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
                    'images',
                    [
                        'behaviour' => [
                            'allowLanguageSynchronization' => true,
                        ],
                        'appearance' => [
                            'createNewRelationLinkTitle' => 'LLL:EXT:frontend/locallang_ttc.xlf:images.addFileReference',
                            'showPossibleLocalizationRecords' => true,
                            'showRemovedLocalizationRecords' => true,
                            'showAllLocalizationLink' => true,
                            'showSynchronizationLink' => true,
                        ],
                        'foreign_match_fields' => [
                            'fieldname' => 'images',
                            'tablenames' => $table,
                            'table_local' => 'sys_file',
                        ],
                        'overrideChildTca' => [
                            'types' => [
                                \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
                                    'showitem' => '
                                    --palette--;;imageoverlayPalette,
                                    --palette--;;filePalette',
                                ],
                            ],
                        ],
                    ],
                    $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']
                ),
            ],
            'price' => [
                'exclude' => true,
                'label' => "$ll:tx_ricoproducts_domain_model_product.price",
                'config' => [
                    'type' => 'input',
                    'size' => 30,
                    'eval' => 'double2',
                ],
            ],
            'attributes' => [
                'exclude' => true,
                'label' => "$ll:$table.attributes",
                'config' => [
                    'type' => 'inline',
                    'foreign_table' => 'tx_ricoproducts_domain_model_attributedata',
                    'foreign_table_where' => 'tx_ricoproducts_domain_model_attributedata.sys_language_uid IN (0,-1)',
                    'foreign_field' => 'variant',
                    'maxitems' => 999,
                    'appearance' => [
                        'collapseAll' => true,
                        'levelLinksPosition' => 'top',
                        'showSynchronizationLink' => true,
                        'showPossibleLocalizationRecords' => true,
                        'showAllLocalizationLink' => true,
                    ],
                    'behaviour' => [
                        'allowLanguageSynchronization' => true,
                    ],
                ],
            ],
            'product' => [
                'config' => [
                    'type' => 'passthrough',
                ],
            ],
        ],
    ];
})(
    \Riconet\RicoProducts\Constants::EXTENSION_KEY,
    'tx_ricoproducts_domain_model_variant'
);
