<?php

/**
 * This file is part of the "rico_products" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2022 PSVneo GmbH
 */

declare(strict_types=1);

namespace Riconet\RicoProducts\Domain\Model;

use TYPO3\CMS\Extbase\Annotation\ORM\Cascade;
use TYPO3\CMS\Extbase\Annotation\ORM\Lazy;
use TYPO3\CMS\Extbase\Annotation\Validate;
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

class Variant extends AbstractEntity
{
    protected ?string $title = '';

    protected ?string $subtitle = '';

    /**
     * @Validate("NotEmpty")
     */
    protected string $articleNumber = '';

    protected ?string $description = '';

    protected ?float $price = 0.00;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference>|null
     *
     * @Cascade("remove")
     *
     * @Lazy()
     */
    protected $images;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Riconet\RicoProducts\Domain\Model\AttributeData>|null
     *
     * @Cascade("remove")
     *
     * @Lazy()
     */
    protected $attributes;

    /**
     * @var \Riconet\RicoProducts\Domain\Model\Product|null
     */
    protected ?Product $product = null;

    public function __construct()
    {
        $this->initStorageObjects();
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): void
    {
        $this->title = $title;
    }

    public function getArticleNumber(): string
    {
        return $this->articleNumber;
    }

    public function setArticleNumber(string $articleNumber): void
    {
        $this->articleNumber = $articleNumber;
    }

    public function getSubtitle(): ?string
    {
        return $this->subtitle;
    }

    public function setSubtitle(?string $subtitle): void
    {
        $this->subtitle = $subtitle;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(?float $price): void
    {
        $this->price = $price;
    }

    public function getImages(): ?ObjectStorage
    {
        return $this->images;
    }

    public function setImages(?ObjectStorage $images): void
    {
        $this->images = $images;
    }

    public function getAttributes(): ?ObjectStorage
    {
        return $this->attributes;
    }

    public function setAttributes(?ObjectStorage $attributes): void
    {
        $this->attributes = $attributes;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): void
    {
        $this->product = $product;
    }

    protected function initStorageObjects(): void
    {
        $this->attributes = new ObjectStorage();
        $this->images = new ObjectStorage();
    }
}
