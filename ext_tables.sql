CREATE TABLE tx_ricoproducts_domain_model_product (
    title varchar(255) DEFAULT '' NOT NULL,
    path_segment varchar(2048),
    subtitle varchar(255) DEFAULT '' NOT NULL,
    article_number varchar(255) DEFAULT '' NOT NULL,
    teaser text,
    description text,
    images int(11) UNSIGNED DEFAULT 0 NOT NULL,
    plan_images int(11) UNSIGNED DEFAULT 0 NOT NULL,
    files int(11) UNSIGNED DEFAULT 0 NOT NULL,
    data_sheets int(11) UNSIGNED DEFAULT 0 NOT NULL,
    manuals int(11) UNSIGNED DEFAULT 0 NOT NULL,
    attributes int(11) UNSIGNED DEFAULT 0 NOT NULL,
    prices int(11) UNSIGNED DEFAULT 0 NOT NULL,
    price double(11, 2) DEFAULT 0.00 NOT NULL,
    variants int(11) UNSIGNED DEFAULT 0 NOT NULL,
    categories int(11) UNSIGNED DEFAULT 0 NOT NULL,
    accessories int(11) UNSIGNED DEFAULT 0 NOT NULL
);

CREATE TABLE tx_ricoproducts_domain_model_variant (
    title varchar(255) DEFAULT '' NOT NULL,
    path_segment varchar(2048),
    subtitle varchar(255) DEFAULT '' NOT NULL,
    description text,
    article_number varchar(255) DEFAULT '' NOT NULL,
    images int(11) UNSIGNED NOT NULL DEFAULT 0,
    prices int(11) UNSIGNED DEFAULT 0 NOT NULL,
    price double(11, 2) DEFAULT 0.00 NOT NULL,
    attributes int(11) UNSIGNED DEFAULT 0 NOT NULL,
    product int(11) UNSIGNED DEFAULT 0 NOT NULL
);

CREATE TABLE tx_ricoproducts_domain_model_attribute (
    title varchar(255) DEFAULT '' NOT NULL,
    unit varchar(255) DEFAULT '' NOT NULL
);

CREATE TABLE tx_ricoproducts_domain_model_attributedata (
    product int(11) UNSIGNED DEFAULT 0 NOT NULL,
    variant int(11) UNSIGNED DEFAULT 0 NOT NULL,
    attribute int(11) UNSIGNED DEFAULT 0 NOT NULL,
    data varchar(255) DEFAULT '' NOT NULL
);

CREATE TABLE tx_ricoproducts_product_product_mm (
    uid_local int(11) UNSIGNED DEFAULT 0 NOT NULL,
    uid_foreign int(11) UNSIGNED DEFAULT 0 NOT NULL,
    sorting int(11) UNSIGNED DEFAULT 0 NOT NULL,
    sorting_foreign int(11) UNSIGNED DEFAULT 0 NOT NULL,
    KEY uid_local (uid_local),
    KEY uid_foreign (uid_foreign)
);
