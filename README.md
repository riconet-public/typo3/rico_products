# Products

### Dependencies
TYPO3 `10.4.0-10.4.99`

## Overview
This extension provides a basic system to manage product data.

## Installation
- Activate the extension via the Extensions module.
- Include the static typo script file.
