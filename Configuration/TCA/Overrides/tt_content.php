<?php

/**
 * This file is part of the "rico_products" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2020 PSVneo
 */

declare(strict_types=1);

defined('TYPO3_MODE') || die();

(function ($extensionKey) {
    // Register product listing plugin.
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
        $extensionKey,
        \Riconet\RicoProducts\Constants::PLUGIN_NAME_PRODUCT_LISTING,
        "LLL:EXT:$extensionKey/Resources/Private/Language/locallang_db.xlf:plugin.product_listing",
        '',
        $extensionKey
    );
    // Register product show plugin.
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
        $extensionKey,
        \Riconet\RicoProducts\Constants::PLUGIN_NAME_PRODUCT_SHOW,
        "LLL:EXT:$extensionKey/Resources/Private/Language/locallang_db.xlf:plugin.product_show",
        '',
        $extensionKey
    );
    // Add custom settings (flex form) for product listing plugin.
    \Riconet\RicoProducts\Utility\PluginUtility::registerFlexForm(
        $extensionKey,
        'Configuration/FlexForms/ProductListing.xml',
        \Riconet\RicoProducts\Constants::PLUGIN_NAME_PRODUCT_LISTING
    );
}
)('rico_products');
